#!/bin/bash

if [ $1 ]; then
    SIZE_BYTES=$1
else
    FILE=`ls -ltra | tail -1 | awk '{ print $9 }'`
    if echo ${FILE} | grep -i VOB; then
        SIZE_BYTES=1073739776
        echo VOB detected, setting filesize to: $SIZE_BYTES. Provide size to override.
    else
        SIZE_BYTES=0
    fi
fi

if [ $2 ]; then
    SLEEP=$2
else
    SLEEP=10
fi


I=0; N=1; 
FIRST=`ls -ltra | tail -1 | awk '{ print $5 }'`
FILE=`ls -ltra | tail -1 | awk '{ print $9 }'`
ARIND=0

while true; do 
    echo $N; 
    ls -ltra | tail -10 ;

    sleep 1; 

    let I="$I+1"; 
    let N="$N+1"; 

    if [ $I -gt 9 ]; then
        LAST=`ls -ltra | tail -1 | awk '{ print $5 }'`
        NEW_FILE=`ls -ltra | tail -1 | awk '{ print $9 }'`
        if [ "${NEW_FILE}" != "${FILE}" ]; then
            ARIND=0
            LAST=0
            TOTAL=`ls -ltra | tail -1 | awk '{ print $5 }'`
            FILE=${NEW_FILE}
        else
            let TOTAL="$LAST - $FIRST"
        fi

        echo got $TOTAL 
        ARRAY[$ARIND]=$TOTAL

        # print average download rate so far per sec
        X=0
        Y=0
        while [ $X -le $ARIND ]; do
            let Y="$Y+${ARRAY[$X]}"
            let X="$X+1"
        done
        SPEED_NOW=`ftc "$TOTAL / 10000"` 
        echo this frame"   ": ${SPEED_NOW} kb/s
        echo -n average speed:" "
        let Z="$ARIND+1"
        X=`ftc "$Y / $Z"`
        X=`ftc "$X / 10000"`
        echo -n "$X kb/sec over $Z, 10 second frames "

        if [ ${SIZE_BYTES} -ne 0 ] ; then
            BYTES_LEFT=`ftc "${SIZE_BYTES} - ${LAST}"`
            R=`ftc "${BYTES_LEFT} / 1000"`
            R=`ftc "$R / ${SPEED_NOW}"`
            R=`ftc "$R / 60"`
            R=`echo $R | awk '{d=sprintf("%d",$1);f=sprintf("%d",($1-d)*60);printf("%d:%02d",d,f);}'`
            echo "($R remaining)"
        else
            echo 
        fi

        let ARIND="$ARIND+1"
        echo -n sleep $SLEEP..." "
        I=1; 
        while [ $I -le $SLEEP ]; do
            echo -n $I" "
            sleep 1
            let I="$I+1"; 
        done
        I=0;
        echo done
        FIRST=`ls -ltra | tail -1 | awk '{ print $5 }'`
    fi; 
done


