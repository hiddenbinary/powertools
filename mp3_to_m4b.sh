#!/bin/bash

#
# IMPORTANT:
#   you need to download faac source, and compile with mp4v2 support
#   for the mp4 wrappers (m4a, m4b, m4v) "./configure --with-mp4v2"
#   faad2-2.7 works.  https://sourceforge.net/projects/faac/files/faad2-src/faad2-2.7/
#
# How to use:
#   1) use mpg123 - to determine your input: CHANNELS, SAMPLE_SIZE, SAMPLE_RATE
#   2) edit the global variables
#   3) run 1 or 2 times with LIMIT=1 to test the Quality Setting
#   4) run with LIMIT=0 to transcode the entire directory
#

SLEEP_BETWEEN_TRACKS=1.66
FAAC="$HOME/code/EXTERN/FAAC/faac-1.28/frontend/faac"
INPUT_SED="sed -E"
NUMBER_PAD_LENGTH=3

## -----------------------------------------------------------------------------
## --------- generally, must edit everything below this line
## -----------------------------------------------------------------------------

SKIP=0      # 0 disables, set this to a positive number to skip over this many
LIMIT=0     # 0 disables, set this positive number to limit how many are run

SOURCE_DIR=/big3/audio/Alexandre\ Dumas\ -\ The\ Last\ Cavalier\ -\ mp3
PERCENT_TOTAL_QUALITY=65
INPUT_CHANNELS=2            # 1 = mono, 2 = joint-stereo, 2 = stereo
INPUT_SAMPLE_SIZE=16        # <--really, this never changes
INPUT_SAMPLE_RATE=22050     # generally 11025, 22050, 44100, 48000

# this is the text sed removes to get the track-number
# text BEFORE the track number
INPUT_PREFIX_NOT_TRACK='\ \w+\ [0-9-]+'
INPUT_PREFIX_NOT_TRACK='TheStoryofEarthPart'
INPUT_PREFIX_NOT_TRACK='Chapter '
INPUT_PREFIX_NOT_TRACK='Last Cavalier - '

# text AFTER the track number
INPUT_SUFFIX_NOT_TRACK='_.*.mp3$'
INPUT_SUFFIX_NOT_TRACK=' of 140 Casca The Persian.mp3'
INPUT_SUFFIX_NOT_TRACK='.mp3'
INPUT_SUFFIX_NOT_TRACK=' .*\.mp3'
INPUT_SUFFIX_NOT_TRACK='.mp3'

OUTPUT_DIR="/big4/audio/dumas_last_cavalier-downsampled"
OUTPUT_FILENAME_PREFIX="Alexandre_Dumas-The_Last_Cavalier-"
OUTPUT_ARTIST="Alexandre Dumas"
OUTPUT_ALBUM="The Last Cavalier"
OUTPUT_COMMENT=""
OUTPUT_GENRE=Audiobooks
OUTPUT_YEAR=2007

#uncomment these to afix an image
#COVER_IMAGE_PATH="${SOURCE_DIR}/thumbnail.jpeg"
#COVER_ART=''
#COVER_ART="--cover-art \"${COVER_IMAGE_PATH}\""
#echo "Using Thumbnail \"${COVER_IMAGE_PATH}\""

###
#=============================================================================
#

if [ ! -d "${OUTPUT_DIR}" ]; then
    echo
    echo "   mkdir -p ${OUTPUT_DIR}"
    echo ; sleep 1
    mkdir -p "${OUTPUT_DIR}"
fi

function pad() {
    local TRACK=$1
    local N=${2:-$NUMBER_PAD_LENGTH}
    while [ ${#TRACK} -lt $N ]; do
        TRACK="0$TRACK"
    done
    echo $TRACK
}

TRACK_NUM=1

for FILE in "${SOURCE_DIR}"/*.mp3; do
    if [ $SKIP -gt 0 ]; then
        let SKIP="$SKIP-1"
        continue
    fi

    echo
    echo "INPUT:"
    echo "  ${FILE}"

    BASE=`basename "${FILE}"`
    if [ "x${INPUT_PREFIX_NOT_TRACK}" == "x" ]; then
        TRACK=`echo "${BASE}" | ${INPUT_SED} s/"${INPUT_SUFFIX_NOT_TRACK}"//g`
    elif [ "x${INPUT_SUFFIX_NOT_TRACK}" == "x" ]; then
        TRACK=`echo "${BASE}" | ${INPUT_SED} s/"${INPUT_PREFIX_NOT_TRACK}"//g`
    else
        TRACK=`echo "${BASE}" | ${INPUT_SED} s/"${INPUT_SUFFIX_NOT_TRACK}"//g | ${INPUT_SED} s/"${INPUT_PREFIX_NOT_TRACK}"//g`
    fi

    TRACK=`pad ${TRACK}`

# uncomment this to generate our own track numbers, instead of stripping them from the audio file
#    TRACK=`pad ${TRACK_NUM}`
    let TRACK_NUM="$TRACK_NUM+1"

    NEWNAME=${OUTPUT_FILENAME_PREFIX}-${TRACK}.m4b
    OUTPUT_FILEPATH="${OUTPUT_DIR}/${NEWNAME}"

    echo
    echo "OUTPUT:"
    echo "  ${OUTPUT_FILEPATH}"
    echo
    sleep $SLEEP_BETWEEN_TRACKS

    lame -t --decode "${FILE}" -x - | \
        ${FAAC} -R ${INPUT_SAMPLE_RATE} -C ${INPUT_CHANNELS} -B ${INPUT_SAMPLE_SIZE} \
         -q $PERCENT_TOTAL_QUALITY -w -s \
         --artist "${OUTPUT_ARTIST}" --album "${OUTPUT_ALBUM}" --track "${TRACK}" --genre "${OUTPUT_GENRE}" --year "${OUTPUT_YEAR}" \
         --comment "${OUTPUT_COMMENT}" ${COVER_ART} -o "${OUTPUT_FILEPATH}" -

    let LIMIT="$LIMIT-1"
    if [ $LIMIT -eq 0 ]; then
        exit 0
    fi
done

