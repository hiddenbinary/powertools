#!/bin/bash
# This comes in handy when configuring cooling fans.
# Hard drives perform best (fail least) when held within the
#  temperature range of 25-45C, below 25C and above 45C led
#  to higher failures. http://research.google.com/archive/disk_failures.pdf

for DRIVE in $(lsblk | awk '$0 ~ /^sd[a-z]/ {print $1}' | sort); do
        DEV=/dev/${DRIVE}
        echo -n "${DEV} "
        ((sudo smartctl --xall ${DEV} | grep Current\ Temp) || echo "") | awk '$0 !~ /^0x05/ {print $0}' | awk '{print $1, $2, $3}'
        sleep 2
done
