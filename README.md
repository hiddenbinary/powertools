# powertools

Collection of useful scripts, tools and utilities I have written over the past decade to help me with an assortment of daily tasks. Most of these are too small to deserve their own repository, but are still incredibly useful.

### Descriptions

| script | description |
| --- | --- |
| clock | simple, legible clock that runs in the terminal |
| countdown_timer.sh | set a time (in seconds), it counts down. An alarm goes off once timer has expired |
| counter | simple counter |
| download-podcast.py | very rough script that provides the basic pythonish idioms for downloading an podcast RSS file and then downloading all the podcast episodes |
| ftc | fast two-argument calculator. Relies on python3. The commandline needed a better, more robust calculator. I have rewritten this multiple times over the years. The first one was written in C. Despite its name, it takes an arithmetical expression of arbitrary type and returns the answer. For convenience it supports 'x' as the multiplication operator. This because most shells expand the asterix '*' |
| fullpath | takes file arguments and returns their expanded full path |
| ls_rand | fun little thing that prints a single random file from the current directory |
| mp3_to_m4b.sh | powertool for converting an homogeneous folder of mp3 files into m4b. I had written this initially because itunes support for m4b at the time was superior. |
| my_ip_address.sh | returns ip address of machine you are on |
| rescape | escape a file path in the same fashion of the Unix shell. Works best (or perhaps the only way) with STDIN. eg. `echo "/holy mountain (1973)" \| rescape` returns: `/holy\ mountain\ \(1973\)` |
| screens | indispensible. Simple wrapper that starts arbitrary number of screen sessions configured to use ~/.bash_profile |
| showsize | also indispensible. Use this to poke around and learn the sizes of directories |
| vid | This may be the most useful thing I have ever written. It is a script front-end for mplayer. The usage idiom is `vid movie.mp4 movie2.avi`. Runs arbitrary number of video files in a light-weight, configurable manner. |
| youtube_download.py | Hehe. I'm sure Alphabet's license says they own the rights to everything uploaded to their site, but come on... we all know this is hogwash. Any site that restricts access to hard files is a farce, a mere ploy to control the flow of information. History is on my side here. If something is important, you can't trust them not to take it down and then lie and say there was a complaint. I have seen this happen several times. Government blacklists and shadow banning is removing all kinds of clandestine and subversive material from internet content sites. We shouldn't be trusting the man to maintain our intellectual heritage for us. Unless of course you are comfortable with Silicon Valley and Capital Hill deciding what is right and what is wrong. |
| convert_textfile_to_mp3s.sh | takes a large text file and line numbers to slice up the file into mp3 files. Use this to turn a text file into an audiobook. This uses another project I wrote: [Nanotts](https://github.com/gmn/nanotts), a text to speech synthesizer |
| iching | comprehensive script for the consultatioon of the iching. It even stores a log of your previous queries. It took me weeks to type in all the changing lines. pretty much everything you need to consult the iching is here, although it's still better to have the book for the extensive commentaries that explain everything |
| duck | shortcut to duckduckgo |
| lookup | shortcut to looking up word definitions on tfd.com |
| wiki | commandline wikipedia query |
| checksumdir | report the checksum for an entire directory |
| check-hd-temps.hd | report the temperatures of all your hard drives. This [Google Paper](http://research.google.com/archive/disk_failures.pdf) shows the optimal temperature range for a hard drive is 25C-45C. |

