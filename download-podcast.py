
import json
from os.path import isdir, isfile, join, dirname, os
import requests
import sys
import time

FEED_URL = "https://rss.art19.com/ram-dass-here-and-now"
FILES_DIR = './FILES'


p = lambda s: print(s)
perr = lambda s: print(s, file=sys.stderr)


def download_file( url, filename ):
    X = requests.get( url )
    with open( filename, 'wb' ) as f:
        f.write(X.content)


def str_indexes( needle, haystack ):
    """ returns list of all indexes of needle found in haystack """
    nlen = len(needle)
    res = []
    index = 0

    while True:
        try:
            i = haystack.index( needle, index )
        except:
            break
        res.append(i)
        index = i + nlen
    return res


def download_rss_xml(url):
    result = requests.get(url)
    feed = result.content.decode('utf-8')
    name = url[url.rindex('/')+1:]
    return feed, name


def rss_xml_from_file(name):
    out = None
    with open(name, "r") as f:
        out = f.read()
    return out, name


def convert_iso_date(iso):
    mons = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    my = iso.split(' ')[2:4]
    i = mons.index(my[0]) + 1
    a = str(i)
    while len(a) < 2:
        a = '0' + a
    return '{}-{}'.format(my[1], a)


def numpad(n, leading_zeros=3):
    s = str(n)
    while len(s) < leading_zeros:
        s = '0' + s
    return s


def main():
    if not isdir(FILES_DIR):
        perr('creating {}'.format(FILES_DIR))
        os.mkdir(FILES_DIR, mode=(7*8**2 + 5*8**1 + 5*8**0)) #755
    else:
        perr('{} exists'.format(FILES_DIR))

    fetch_remote = False

    if fetch_remote:
        p('downloading Ram Dass Here and Now RSS feed')
        feed, feed_name = download_rss_xml(FEED_URL)
        p('updating --> "{}"'.format(feed_name))
        with open(feed_name,'w') as f:
            f.write(feed)
    else:
        p('loading Ram Dass Here and Now RSS feed from file')
        FEED_NAME = FEED_URL[FEED_URL.rindex('/')+1:]
        feed, feed_name = rss_xml_from_file(FEED_NAME)

    time.sleep( (2 ** 0.5) )

    #
    # get all the individual episode dates
    #
    sentinel = '<pubDate>'
    date_indexes = str_indexes( sentinel, feed )
    DATES = []
    for index, start in enumerate(date_indexes):
        start += len(sentinel)
        end = feed.index('<', start)
        assert(feed[end] == '<')
        dstring = feed[start:end].rstrip().lstrip()
        DATES.append(convert_iso_date(dstring))
    DATES.reverse()

    sentinel = '<enclosure url="'
    mp3_url_indexes = str_indexes( sentinel, feed )

    print('{} files to check'.format(len(mp3_url_indexes)))

    #
    # get all the direct mp3 urls
    #
    URLS = []
    for index, start in enumerate(mp3_url_indexes):
        start += len(sentinel)
        end = feed.index('"', start)
        assert(feed[start] == 'h')
        assert(feed[end] == '"')
        url = feed[start:end].rstrip().lstrip()
        URLS.append(url)
    URLS.reverse()

    #
    # get all files we dont already have
    #
    for index, url in enumerate(URLS):
        # output name
        relname = join(FILES_DIR, '{}-ram-dass-podcast-{}.mp3'.format(numpad(index+1), DATES[index]))

        # check if file exists
        if isfile(relname):
            p('{}  --->  exists'.format(relname))
            time.sleep( (2 ** 0.5) / 8 / 2 )
        else:
            # get it
            p( 'getting "{}" --> "{}"'.format(url, relname) )
            download_file( url, relname )
            if isfile( relname ):
                p('success: saved "{}"'.format(relname))
            else:
                p('{} download failed [{}]'.format(relname, url))
                break
            time.sleep( 2 )


if __name__ == '__main__':
    main()
