#!/bin/bash

function hms() {
    local SEC=$1
    HOUR=$(( $SEC / 3600 ))
    HS=$(( $HOUR * 3600 ))
    SEC=$(( $SEC - $HS ))
    MIN=$(( $SEC / 60 ))
    MS=$(( $MIN * 60 ))
    SEC=$(( $SEC - $MS ))
    if [ ${#HOUR} -lt 2 ]; then HOUR=0$HOUR ; fi
    if [ ${#MIN} -lt 2 ]; then MIN=0$MIN ; fi
    if [ ${#SEC} -lt 2 ]; then SEC=0$SEC ; fi
    echo "$HOUR:$MIN:$SEC"
}

function countdown() {
    LEN=${1:-60}
    NEWLINE=${2:-""}
    if [ "x$NEWLINE" != "x" ]; then
        ECHO_CMD="echo -n"
        echo 'updating on same line'
    else
        ECHO_CMD="echo"
        echo 'updating on a new line'
    fi

    N=$LEN
    while [ $N -gt 0 ]; do
        FORMATTED=`hms $N`
        ${ECHO_CMD} "${FORMATTED} "
        let N="$N-1"
        sleep 1
    done
    echo
}

function alarm() {
    N=0
    while :; do
        echo
        echo '*************************************************'
        echo -n ' !!ALARM ~ Countdown Finished ~ ALARM!! '
        hms $N
        let N="$N+2"
        echo '*************************************************'
        sleep 1
        echo
        sleep 1
    done
}

countdown $1 $2
alarm
exit 0

