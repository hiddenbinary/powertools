#
# Setup your Python packages:
#   python3 -m venv venv
#   . ./venv/bin/activate
#   python3 -m pip install --upgrade pip
#   python3 -m pip install youtube_dl pafy
#

import json
import pafy
import sys
import time
import math

legit_opts = ['-Ba', '-Bv', '-m4a', '-mp4'] + [str(x) for x in range(1,10)]


def fixname(n, a, i, e):
    return '{} - {} - {}.{}'.format(n, a, i, e).replace('|', '--').replace('/','-').replace('\\', '-').replace(':', ' - ').replace('"',"'")


def download_stream(S, t, a, i):
    fixedname = fixname(t, a, i, S.extension)
    print("\n")
    print('downloading: "{}"'.format(fixedname))
    S.download(filepath=fixedname)


def get_video(url, index_opt=None):

    video = pafy.new(url)
    print("VIDEO: \"{}\"\nAUTHOR: {}\nURL: {}\nVIEWS: {}".format(video.title, video.author, video.watchv_url, video.viewcount))

    print("\nVideo Streams:")
    stream_index = 1
    streams = ["null"]
    for stream in video.streams:
        print(" - [[{}]]: {}, {}".format(stream_index, stream, stream.get_filesize()))
        streams.append(stream)
        stream_index += 1

    print("\nAudio Streams:")
    for stream in video.audiostreams:
        print(" - [[{}]]: {}, {}, {}".format(stream_index, stream.bitrate, stream.extension, stream.get_filesize()))
        streams.append(stream)
        stream_index += 1

    if not index_opt:
        print("\n** Add index option {} to download a stream\n".format(legit_opts[:stream_index+3]))
        return None

    stream_toget = None

    # index argument
    if ord(index_opt[0]) >= ord('0') and ord(index_opt[0]) <= ord('9') and len(index_opt) == 1:
        indices = int(index_opt)
        stream_toget = streams[indices]
    else:
        if index_opt == '-Ba':
            stream_toget = video.getbestaudio()
        elif index_opt == '-Bv':
            stream_toget = video.getbest()
        elif index_opt == '-m4a' or index_opt == '-mp4':
            found = False
            for S in video.allstreams:
                if S.extension == index_opt[1:]:
                    stream_toget = S
                    found = True
            if not found:
                print('valid {} stream not found'.format(index_opt[1:]))

    if stream_toget:
        download_stream(stream_toget, video.title, video.author, video.videoid)


def usage():
    print('usage: {} <youtube url> [stream index]|[-Bv|-Ba|-m4a]'.format(sys.argv[0]))
    print('       [1-9] stream index to download')
    print('       -mp4  get the first mp4 video')
    print('       -m4a  get the first m4a audio')
    print('       -Bv   best video')
    print('       -Ba   best audio')


if __name__ == "__main__":
    print(json.dumps(sys.argv))

    if '-h' in sys.argv or '--help' in sys.argv:
        usage()

    elif len(sys.argv) > 1:
        vids = []
        actual_opt = None
        for arg in sys.argv[1:]:
            found_opt = False
            for opt in legit_opts:
                if opt == arg:
                    actual_opt = opt
                    found_opt = True
            if not found_opt:
                vids.append(arg)

        for index, vid in enumerate(vids):
            print("\n{}".format("~"*60))
            print("Getting video {} of {}".format(index+1, len(vids)))
            get_video(vid, actual_opt)
            time.sleep(math.e)

    else:
        usage()

