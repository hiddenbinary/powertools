#!/bin/bash

print_hexagram_lines() {
    FOUND=0
    LINE=1
    DESIRED_LINE=${2:-0}
    while read line; do
#echo "LINE: $line"
        if [ $FOUND -gt 0 ]; then
            if [ "x$line" == "x" ]; then
                ((LINE=LINE+1))
                if [ $LINE -gt $DESIRED_LINE ] || [ $LINE -gt 6 ]; then
                    break;
                fi
#                echo # output empty line
            elif [ "x$DESIRED_LINE" == "x$LINE" ]; then
                echo "$line"
            fi
            continue
        fi

        if [ "X${line:0:1}" == "X#" ]; then
            if [ "x$line" == "x# hexagram $1" ]; then
                FOUND=1
                echo "#"
                echo "# outputting Hexagram $1, line $DESIRED_LINE"
                echo "#"
                continue
            fi
        fi
    done < iching_lines
}

if [ ${#} -gt 0 ]; then
    print_hexagram_lines $1 $2
else
    echo "Usage: iching_lines_test <HEXAGRAM_NUMBER> <LINE_NUMBER>"
fi

exit 0
