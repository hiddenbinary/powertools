#!/bin/bash
# Convert the entire digest issue of 2600 volume 35 into audio files for easy listening!

# I have found these settings considerably improve the legibility of the nanotts output; ymmv
speed="0.8"
speed="0.78"
voice="en-US"
volume="0.6"
pitch="1.14"
OVERWRITE_EXISTING_FILES=0

#gleen greenwald
#SECTIONS="80 138 144 476 482 1148 1154 1724 1730 2096 2102 2496 2502 2538 2544 2568"

# your file location and name will vary, obviously.
#FILENAME=Prime\ Green\ _\ Remembering\ the\ Sixties\ -\ Robert\ Stone.txt
#ARTIST="Robert Stone"
#ALBUM="Prime Green, Remembering the Sixties"
#OUTDIR_OPTIONAL="Robert_Stone-Prime_Green_Remembering_The_Sixties"
#SECTIONS="1 66 72 137 143 208 214 285 291 354 360 419 425 486 492 539 541 543" # robert stone

#FILENAME="The River Swimmer - Jim Harrison.txt"
#ARTIST="Jim Harrison"
#ALBUM="The River Swimmer"
#OUTDIR_OPTIONAL="Jim_Harrison-The_River_Swimmer"
#SECTIONS="153 189 195 217 223 321 327 387 393 443 449 507 513 539 545 565 571 599 605 651 657 693 699 727 733 757 763 793 799 825 831 865
#    871 927 933 1273 1279 1431"

#FILENAME=2600_MAGAZINE/2600_\ The\ Hacker\ Digest\ -\ Volume\ 34.txt
#ARTIST="2600 Magazine"
#ALBUM="2600 Vol. 34"
#OUTDIR_OPTIONAL="2600_MAGAZINE_VOL_34"
#SECTIONS="210 234 240 280 286 312 318 336 342 462 468 484 490 502 508 540 546 668 674 724 730 742 748 858 864 1012 1022 1076 1082 1124 1130 1210
#    1218 1232 1238 1260 1266 1434 1444 1522 1528 1552 1558 1588 1594 1634 1640 1648 1656 1676 1682 1710 1716 2006 2012 2210 2216 2244 2250 2280 2286 2314 2320 2364
#    2370 2404 2410 2542 2548 2604 2610 2672 2678 2710 2716 2738 2744 2808 2814 2834 2840 3282 3288 3306 3312 3332 3338 3368 3374 3396 3402 3418 3424 3514 3520 3538
#    3544 3646 3652 3862 3868 3914 3920 3958 3964 4136 4142 4182 4188 4444 4450 4534 4540 4578 4584 4600 4606 4638 4644 4660 4666 4802 4808 4834 4840 4870 4876 4904
#    4910 4962 4968 5052 5058 5170 5176 5246 5252 5280 5286 5302 5308 5330 5336 5374 5380 5566 5572 5624 5630 5698 5704 5720 5726 5750 5756 5884 5890 5908 5914 6062
#    6068 6106 6112 6182 6188 6278 6284 6330 6336 6352 6358 6564 6570 6660 6666 6708 6714 6842 6848 6872 6878 6992 7004 7574 7576 7678 7682 8216 8222 9576"

FILENAME="A Good Day for Seppuku - Kate Braverman.txt"
ARTIST="Kate Braverman"
ALBUM="A Good Day for Seppuku"
OUTDIR_OPTIONAL="A Good Day for Seppuku"
SECTIONS="1 20 3629 3632
    85 144 148 232 236 268 272 300 304 400 404 470 474 512 516 600 606 747 751 989 995 1076 1080 1176 1180 1204 1208 1260 1266 1435
    1441 1768 1774 2121 2127 2812 2818 3623
"

COUNT=1
HEAD=''
TAIL=''

function run_nanotts() {
    local count=$3
    local title="$4"
    local DIR="$5"
    while [ ${#count} -lt 3 ]; do count=0$count; done
    local file="$DIR/$count-$ARTIST-$ALBUM-$title.mp3"

    if [ $OVERWRITE_EXISTING_FILES -ne 1 ] && [ -e "$file" ]; then
        echo skipping $file
        sleep 0.314
    else
        echo "nanotts --speed $speed --volume $volume --pitch $pitch --voice $voice < <( head -$1 \"${FILENAME}\" | tail -$2; echo " . . . . . . " ) -c | lame -r -s 16 -m m -V 0 -b 56 --ta \"$ARTIST\" --tl \"$ALBUM\" --tn $count - \"$file\"" >/dev/stderr
              nanotts --speed $speed --volume $volume --pitch $pitch --voice $voice < <( head -$1 "${FILENAME}" | tail -$2; echo " . . . . . . " ) -c | lame -r -s 16 -m m -V 0 -b 56 --ta "$ARTIST" --tl "$ALBUM" --tn $count - "$file"

        sleep 4
    fi
}

if [ ! -z "$OUTDIR_OPTIONAL" ]; then
    if [ ! -d "$OUTDIR_OPTIONAL" ]; then
        mkdir "$OUTDIR_OPTIONAL"
    fi
else
    $OUTDIR_OPTIONAL="."
fi

for sect in ${SECTIONS}; do
    echo $sect >/dev/stderr
    if [ -z "$TAIL" ]; then
        TAIL=$sect
    else
        HEAD=$sect
        let TAIL="$HEAD-$TAIL+1"

        echo "head -$HEAD "$FILENAME" | tail -$TAIL" >/dev/stderr
              head -$HEAD "$FILENAME" | tail -$TAIL
        echo >/dev/stderr;

        TITLE=`head -$HEAD "$FILENAME" | tail -$TAIL | head -1`

        run_nanotts $HEAD $TAIL ${COUNT} "${TITLE:0:50}" "$OUTDIR_OPTIONAL"

        HEAD=''; TAIL=''
        let COUNT="$COUNT+1"
    fi
done
